# Friendlier Webform Components

Friendlier webform components is a small library that contains custom css for [checkbox, radiobox, select, file upload, and progress] with minimal CSS/HTML changes.

## How to use:

Simply add the role class on the input element and see it working

```html
<input type="checkbox" class="checkbox" />
<input type="radio" class="radio" />
<select class="select">...</select>
<progress class="progress" value="25" max="100">...</progress>
<input type="file" class="file">
```